from itertools import zip_longest

def grouper(iterable, n, fillvalue=None):
    "Collect data into fixed-length chunks or blocks"
    # grouper('ABCDEFG', 3, 'x') --> ABC DEF Gxx"
    args = [iter(iterable)] * n
    return zip_longest(*args, fillvalue=fillvalue)


def main():
    with open('input') as input_data:
        num_valid_triangles = 0
        all_lines = []
        for line in input_data:
            line = line.split()
            all_lines.append([int(line[0]), int(line[1]), int(line[2])])
        for lines in grouper(all_lines, 3):
            if possible_triangle((lines[0][0], lines[1][0], lines[2][0])): 
                num_valid_triangles += 1
            if possible_triangle((lines[0][1], lines[1][1], lines[2][1])): 
                num_valid_triangles += 1
            if possible_triangle((lines[0][2], lines[1][2], lines[2][2])): 
                num_valid_triangles += 1
    print(num_valid_triangles)


def possible_triangle(side_lengths):
    valid_triangle = (
        (side_lengths[0] + side_lengths[1]) > side_lengths[2]
        and (side_lengths[0] + side_lengths[2]) > side_lengths[1]
        and (side_lengths[2] + side_lengths[1]) > side_lengths[0]
        )
    return valid_triangle


if __name__ == '__main__':
    main()
