from collections import defaultdict

def main():
    with open('input') as input_data:
        for line in input_data:
            print('blocks away after all', blocks_away(line))
            print('first to visit twice away', find_first_to_visit_twice(line))


def location_visited_twice(locations_visited):
    for loc, num in locations_visited.items():
        if num > 1:
            return True
    return False


def find_first_to_visit_twice(instructions):
    instructions = instructions.strip().split(', ')
    location = [0, 0]
    # 0 is North & where we start. Add 1 for every right turn. subtract 1
    # for every left turn. So, 1 is East, 2 is South, and 3 is West. Take
    # the modulous to recover the direction
    direction = 0
    locations_visited = defaultdict(int)
    locations_visited[tuple(location)] += 1
    for instruction in instructions:
        turn, num_blocks = instruction[0], instruction[1:]
        num_blocks = int(num_blocks)
        if turn == 'R':
            direction = direction + 1
        elif turn == 'L':
            direction = direction - 1
        else:
            # There should only be R and L
            raise ValueError
        facing = direction % 4
        if facing == 0:
            for _ in range(num_blocks):
                location[1] += 1
                locations_visited[tuple(location)] += 1
                if location_visited_twice(locations_visited):
                    return abs(location[0]) + abs(location[1])
        elif facing == 1:
            for _ in range(num_blocks):
                location[0] += 1
                locations_visited[tuple(location)] += 1
                if location_visited_twice(locations_visited):
                    return abs(location[0]) + abs(location[1])
        elif facing == 2:
            for _ in range(num_blocks):
                location[1] -= 1
                locations_visited[tuple(location)] += 1
                if location_visited_twice(locations_visited):
                    return abs(location[0]) + abs(location[1])
        else:
            # facing = 3:
            for _ in range(num_blocks):
                location[0] -= 1
                locations_visited[tuple(location)] += 1
                if location_visited_twice(locations_visited):
                    return abs(location[0]) + abs(location[1])
    # There should always be a loc visited twice
    raise ValueError



def blocks_away(instructions):
    instructions = instructions.strip().split(', ')
    location = [0, 0]
    # 0 is North & where we start. Add 1 for every right turn. subtract 1
    # for every left turn. So, 1 is East, 2 is South, and 3 is West. Take
    # the modulous to recover the direction
    direction = 0
    for instruction in instructions:
        turn, num_blocks = instruction[0], instruction[1:]
        num_blocks = int(num_blocks)
        if turn == 'R':
            direction = direction + 1
        elif turn == 'L':
            direction = direction - 1
        else:
            # There should only be R and L
            raise ValueError
        facing = direction % 4
        if facing == 0:
            location[1] += num_blocks
        elif facing == 1:
            location[0] += num_blocks
        elif facing == 2:
            location[1] -= num_blocks
        else:
            # facing = 3:
            location[0] -= num_blocks
    return( abs(location[0]) + abs(location[1]))


if __name__ == '__main__':
    main()
