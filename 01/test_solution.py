import pytest

from solution import blocks_away, find_first_to_visit_twice


@pytest.mark.parametrize("instructions, expected_blocks_away", [
    ("R2, L3", 5),
    ("R2, R2, R2", 2),
    ("R5, L5, R5, R3", 12),
])
def test_blocks_away(instructions, expected_blocks_away):
    num_blocks_away = blocks_away(instructions)
    assert num_blocks_away == expected_blocks_away

def test_visit_twice():
    dist_to_first_visited_twice = find_first_to_visit_twice('R8, R4, R4, R8')
    assert dist_to_first_visited_twice == 4


