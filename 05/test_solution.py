import pytest

from solution import find_password, find_positional_password

def test_find_positional_password(door_id=b'abc'):
    password = find_positional_password(door_id)
    assert password == '05ace8e3'

def test_find_password(door_id=b'abc'):
    password = find_password(door_id) 
    assert password == '18f47a30'
