import hashlib
import sys


def find_positional_password(door_id):
    password = [''] * 8
    index = 0
    while not all(password):
        if index % 1_000_000 == 0:
            print(f'On index {index}')
            sys.stdout.flush()
        md5 = hashlib.md5()
        bytes_index = bytes(str(index), encoding='ascii')
        to_hash  = door_id + bytes_index
        md5.update(to_hash)
        digest = md5.hexdigest() 
        if digest.startswith(5 * '0'):
            sys.stdout.flush()
            location = digest[5]
            if location in '01234567':
                location = int(location)
                if password[location] == '':
                    password[location] = digest[6]
                    print('to_hash', digest, location, digest[6])
        index += 1 
    password = ''.join(password)
    print(password)
    return password

def find_password(door_id):
    password = ''
    for index in range(10_000_000):
        if index % 1_000_000 == 0:
            print(f'On index {index}')
            sys.stdout.flush()
        md5 = hashlib.md5()
        bytes_index = bytes(str(index), encoding='ascii')
        to_hash  = door_id + bytes_index
        md5.update(to_hash)
        digest = md5.hexdigest() 
        if digest.startswith(5 * '0'):
            print(to_hash, digest)
            sys.stdout.flush()
            password += digest[5]
            if len(password) == 8:
                return password

def main():
    print(find_positional_password(b'cxdnnyjw'))
    print(find_password(b'cxdnnyjw'))

if __name__ == '__main__':
    main()
