from collections import Counter
import re

SEPARATE = re.compile(r'^(?P<letters>[a-z-]+)-(?P<sector_id>[0-9]+)\[(?P<checksum>[a-z]+)\]$')
ORD_OFFSET = 97


def decrypt(encrypted_room):
    letters, sector_id, checksum = break_apart(encrypted_room)
    decrypted = []
    for char in letters:
        if char == '-':
            decrypted.append(' ')
        else:
            original_char = ord(char) - ORD_OFFSET
            new_character = (original_char + sector_id) % 26 + ORD_OFFSET
            decrypted.append(chr(new_character))
    return ''.join(decrypted)


def calculate_checksum(letters):
    letters = letters.replace('-', '')
    counts = Counter(letters)
    common_letters = counts.most_common()
    common_letters.reverse()
    num_occurances = 0
    group = [] 
    checksum = ''
    for letter, occurances in common_letters:
        if occurances > num_occurances:
            num_occurances = occurances
            if group:
                group.sort()
                checksum = ''.join(group) + checksum
                group = []
        group.append(letter)
    group.sort()
    checksum = ''.join(group) + checksum
    return checksum[0:5]


def break_apart(encrypted_room):
    parts = SEPARATE.match(encrypted_room).groupdict()
    letters = parts['letters']
    sector_id = int(parts['sector_id'])
    checksum = parts['checksum']
    return (letters, sector_id, checksum)


def real_room(encrypted_room):
    letters, sector_id, checksum = break_apart(encrypted_room)
    calculated_checksum = calculate_checksum(letters)
    return (calculated_checksum == checksum, sector_id)


def main():
    with open('input') as input_data:
        total = 0
        for line in input_data:
            is_real, sector_id = real_room(line)
            if is_real:
                total += sector_id
                print('{:5d} - {}'.format(
                        sector_id, decrypt(line)))
    print(total)


if __name__ == '__main__':
    print(decrypt('qzmt-zixmtkozy-ivhz-343[checksum]'))
    main()
