instructions = '''ULL
RRDDD
LURDL
UUUUD'''.split('\n')

from solution import get_code, get_modified_code

def test_get_code():
    assert get_code(instructions) == 1985

def test_second_code():
    assert get_modified_code(instructions) == '5DB3'

