"""Solve the bathroom codes


1 2 3  -1,1  0,1  1,1
4 5 6  -1,0  0,0  1,0
7 8 9  -1,-1 0,-1 1,-1

"""
def get_modified_code(instructions):
    """ Solve now that we know the keypad

        1                 2,2
      2 3 4          1,1  2,1  3,1
    5 6 7 8 9    0,0 1,0  2,0  3,0 4,0
      A B C          1,-1 2,-1 3,-1
        D                 2,-2
    """
    key_decoder = { 
        (2, 2): '1',
        (1, 1): '2',
        (2, 1): '3',
        (3, 1): '4',
        (0, 0): '5',
        (1, 0): '6',
        (2, 0): '7',
        (3, 0): '8',
        (4, 0): '9',
        (1, -1): 'A',
        (2, -1): 'B',
        (3, -1): 'C',
        (2, -2): 'D',
        }
    code = ''
    current_position = (0, 0)
    for line in instructions:
        line = line.strip()
        for instruction in line:
            current_position = follow_one_modified_instruction(
                instruction,
                current_position,
                key_decoder
                )
        code = code + key_decoder[current_position]
    return code 


def get_code(instructions):
    number_coordinates = {
        (-1, 1): '1',
        (0, 1): '2',
        (1, 1): '3',
        (-1, 0): '4',
        (0, 0): '5',
        (1, 0): '6',
        (-1, -1): '7',
        (0, -1): '8',
        (1, -1): '9',
        }
    code = ''
    current_position = (0, 0)
    for line in instructions:
        line = line.strip()
        for instruction in line:
            current_position = follow_one_instruction(
                instruction,
                current_position
                )
        code = code + number_coordinates[current_position]
    return int(code)


def calculate_next_position(move, current_position):
    next_position = tuple(map(sum, zip(current_position, move)))
    if abs(next_position[0]) > 1 or abs(next_position[1]) > 1:
        next_position = current_position
    return next_position


def calculate_modified_next_position(move, current_position, key_decoder):
    next_position = tuple(map(sum, zip(current_position, move)))
    if next_position not in key_decoder:
        next_position = current_position
    return next_position


def follow_one_modified_instruction(instruction, current_position, key_decoder):
    if instruction == 'U':
        move = (0, 1)
    elif instruction == 'D':
        move = (0, -1)
    elif instruction == 'R':
        move = (1, 0)
    elif instruction == 'L':
        move = (-1, 0)
    else:
        # There should always be one of the four
        raise ValueError
    next_position = calculate_modified_next_position(move, current_position, key_decoder)
    return next_position


def follow_one_instruction(instruction, current_position):
    if instruction == 'U':
        move = (0, 1)
    elif instruction == 'D':
        move = (0, -1)
    elif instruction == 'R':
        move = (1, 0)
    elif instruction == 'L':
        move = (-1, 0)
    else:
        # There should always be one of the four
        raise ValueError
    next_position = calculate_next_position(move, current_position)
    return next_position


def main():
    with open('input') as input_data:
#         print(get_code(input_data))
        print(get_modified_code(input_data))


if __name__ == '__main__':
    main()
